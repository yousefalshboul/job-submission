Rails.application.routes.draw do
  devise_for :users
  resources :users, only: [:show, :index] do
    resources :jobs
  end
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  resources :jobs
  root 'jobs#index'
  get 'users/new'
end
